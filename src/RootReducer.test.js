import {rootReducer} from './state/reducer'
import {
    ACTION_TYPE,
    hitIntro,
    hitUndo,
    operation,
    OPERATION_TYPE,
    setNumber,
    setPoint
} from "./state/actions";

describe('rootReducer', function() {

    test('initial State is created', () => {
        let state = rootReducer(undefined, {type:ACTION_TYPE.DO_NOTHING});
        expect(state.display_value).toBe('0');
        expect(state.stack_values.length).toBe(0);
        expect(state.previous_states.length).toBe(0);
    });

    test('set a number', () => {
        let state = rootReducer(undefined, setNumber(2));
        expect(state.display_value).toBe('2');
    });

    test('point is only set once', () => {
        let state = rootReducer(undefined, {type:ACTION_TYPE.DO_NOTHING});
        state = rootReducer(state, setPoint());
        state = rootReducer(state, setPoint());
        expect(state.display_value).toBe('0.');
    });

    test('number pushed to the stack', () => {
        let state = rootReducer(undefined, {type:ACTION_TYPE.DO_NOTHING});
        state = rootReducer(state, setNumber(2));
        state = rootReducer(state, setNumber(2));
        state = rootReducer(state, hitIntro());

        expect(state.display_value).toBe('0');
        expect(state.stack_values.length).toBe(1);
        expect(state.stack_values[0]).toBe(22);
    });

    test('undo change', () => {
        let state = rootReducer(undefined, {type:ACTION_TYPE.DO_NOTHING});
        state = rootReducer(state, setNumber(2));
        state = rootReducer(state, setNumber(2));
        state = rootReducer(state, hitUndo());

        expect(state.previous_states.length).toBe(1);
        expect(state.display_value).toBe('2');
    });

    test('add 2 values', () => {
        let state = rootReducer(undefined, {type:ACTION_TYPE.DO_NOTHING});
        state = rootReducer(state, setNumber(2));
        state = rootReducer(state, hitIntro());
        state = rootReducer(state, setNumber(2));
        state = rootReducer(state, hitIntro());
        state = rootReducer(state, operation(OPERATION_TYPE.ADD));

        expect(state.stack_values.length).toBe(0);
        expect(state.display_value).toBe('4');
    });

    test('minus 2 values', () => {
        let state = rootReducer(undefined, {type:ACTION_TYPE.DO_NOTHING});
        state = rootReducer(state, setNumber(3));
        state = rootReducer(state, hitIntro());
        state = rootReducer(state, setNumber(2));
        state = rootReducer(state, hitIntro());
        state = rootReducer(state, operation(OPERATION_TYPE.MINUS));

        expect(state.stack_values.length).toBe(0);
        expect(state.display_value).toBe('1');
    });

    test('push display and perform sqrt', () => {
        let state = rootReducer(undefined, {type:ACTION_TYPE.DO_NOTHING});
        state = rootReducer(state, setNumber(4));
        state = rootReducer(state, operation(OPERATION_TYPE.SQRT));

        expect(state.stack_values.length).toBe(0);
        expect(state.display_value).toBe('2');
    });

    test('sum all the stack', () => {
        let state = rootReducer(undefined, {type:ACTION_TYPE.DO_NOTHING});

        state = rootReducer(state, setNumber(4));
        state = rootReducer(state, hitIntro());
        state = rootReducer(state, setNumber(3));
        state = rootReducer(state, hitIntro());
        state = rootReducer(state, setNumber(2));
        state = rootReducer(state, hitIntro());
        state = rootReducer(state, setNumber(1));
        state = rootReducer(state, hitIntro());
        state = rootReducer(state, operation(OPERATION_TYPE.SUM));

        expect(state.stack_values.length).toBe(0);
        expect(state.display_value).toBe('10');
    });

    test('sum all the stack2', () => {
        let state = rootReducer(undefined, {type:ACTION_TYPE.DO_NOTHING});
        state = rootReducer(state, setNumber(4));
        state = rootReducer(state, operation(OPERATION_TYPE.SUM));

        expect(state.stack_values.length).toBe(0);
        expect(state.display_value).toBe('4');
    });

});
