export const selectCurrentNumber = (state) => {
  return state.display_value;
};

export const selectCurrentStack = (state) => {
  return state.stack_values;
};
