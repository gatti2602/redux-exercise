export const ACTION_TYPE = {
  SET_NUMBER: 'SET_NUMBER',
  SET_POINT: 'SET_POINT',
  INTRO: 'INTRO',
  OPERATION: 'OPERATION',
  UNDO: 'UNDO',
  DO_NOTHING: 'DO_NOTHING'
}

export const OPERATION_TYPE = {
  SQRT: "SQRT",
  ADD: "ADD",
  MINUS: "MINUS",
  DIVIDE: "DIVIDE",
  MULTIPLY: "MULTIPLY",
  SUM: "SUM"
};

export const doNothing = () => ({
  type: ACTION_TYPE.DO_NOTHING,
});

export const setNumber = (number) => ({
  type: ACTION_TYPE.SET_NUMBER,
  number: number,
});

export const setPoint = () => ({
  type: ACTION_TYPE.SET_POINT,
});

export const hitIntro = () => ({
  type: ACTION_TYPE.INTRO,
});

export const operation = (operation) => ({
  type: ACTION_TYPE.OPERATION,
  operationType: operation
});

export const hitUndo = () => ({
  type: ACTION_TYPE.UNDO,
});
