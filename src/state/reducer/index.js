import {ACTION_TYPE, hitIntro, OPERATION_TYPE} from "../actions";

const INITIAL_VALUE = '0';

const initialState = {
    display_value: INITIAL_VALUE,
    stack_values: [],
    previous_states: []
};

export const rootReducer = (state = initialState, action) => {

    let newState = Object.assign({}, state);

    switch (action.type) {
        case ACTION_TYPE.SET_NUMBER:
            newState = pushState(state);
            if (state.display_value !== INITIAL_VALUE)
                newState.display_value = state.display_value.concat(action.number.toString())
            else
                newState.display_value = action.number.toString();
            break;
        case ACTION_TYPE.SET_POINT:
            if (!state.display_value.includes('.')){
                newState = pushState(state);
                newState.display_value = state.display_value.concat('.');
            }
            break;
        case ACTION_TYPE.INTRO:
            newState = pushState(state);
            newState.stack_values.push(parseFloat(state.display_value));
            newState.display_value = INITIAL_VALUE;
            break;
        case ACTION_TYPE.OPERATION:
            newState = getOperation(action.operationType)(state);
            break;
        case ACTION_TYPE.UNDO:
            newState = popState(state);
            break;
        default:
            break;
    }
    return newState;
};

const pushState = (state) => {
    let newState = Object.assign({}, state);
    let curState = {display_value: state.display_value, stack_values: [...(state.stack_values)]};
    newState.previous_states.push(curState);
    return newState;
};

const popState = (state) => {
    let newState = Object.assign({}, state);
    if (newState.previous_states.length > 0) {
        let curState = newState.previous_states.pop();
        newState.display_value = curState.display_value;
        newState.stack_values = curState.stack_values;
    }
    return newState;
};

/***
 * Receives and operation, validates there are enough parameters on the stack (and also push the display if needed)
 * Then if everything is ok executes the operation
 * @param operation operation should take 1 to n(rest) parameters
 * @returns {function(*=): *}
 */
const validateAndApplyOperation = (operation) => {
    return (state) => {
        let newState = Object.assign({}, state);

        //paramCount will have the whole stack if received parameters are 0
        let paramCount = operation.length === 0 ? newState.stack_values.length : operation.length;

        if (state.display_value !== INITIAL_VALUE && state.stack_values.length >= (paramCount - 1)){
            newState = rootReducer(state, hitIntro());
            paramCount = paramCount === 0 ? paramCount + 1 :paramCount; //If paramcount was stack length then increase
        }


        if (newState.stack_values.length >= paramCount && paramCount > 0) {
            let params = []
            newState = pushState(newState);
            for (let i = 0; i < paramCount; i++) {
                params.push(newState.stack_values.pop());
            }
            newState.display_value = operation(...params).toString();
        }
        return newState;
    };
}

const getOperation = (operation) => {
    switch (operation) {
        case OPERATION_TYPE.SQRT:
            return validateAndApplyOperation((a) => Math.sqrt(a));
        case OPERATION_TYPE.ADD:
            return validateAndApplyOperation((a, b) => (a + b));
        case OPERATION_TYPE.SUM:
            return validateAndApplyOperation((...a) => (a.reduce((acum, curr) => acum + curr)));
        case OPERATION_TYPE.MINUS:
            return validateAndApplyOperation((a, b) => (b - a));
        case OPERATION_TYPE.DIVIDE:
            return validateAndApplyOperation((a, b) => (b / a));
        case OPERATION_TYPE.MULTIPLY:
            return validateAndApplyOperation((a, b) => (a * b));
        default:
            return (state) => {
                return Object.assign({}, state);
            };
    }
};

