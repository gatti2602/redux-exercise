import { useDispatch, useSelector } from 'react-redux';

import {doNothing, hitIntro, hitUndo, operation, OPERATION_TYPE, setNumber, setPoint} from 'state/actions';
import { selectCurrentNumber, selectCurrentStack } from 'state/selectors';

import styles from './Calculator.module.css';

const renderStackItem = (value, index) => {
  return <div key={index}>{value}</div>;
};

export const Calculator = () => {
  const currentNumber = useSelector(selectCurrentNumber);
  const stack = useSelector(selectCurrentStack);
  const dispatch = useDispatch();
  const dispatchAction = (action = doNothing()) => {
    dispatch(action);
  };

  return (
    <div className={styles.main}>
      <div className={styles.display}>{currentNumber}</div>
      <div className={styles.numberKeyContainer}>
        {[...Array(9).keys()].map((i) => (
          <button key={i} onClick={() => dispatchAction(setNumber(i + 1))}>
            {i + 1}
          </button>
        ))}
        <button className={styles.zeroNumber} onClick={() => dispatchAction(setNumber(0))}>
          0
        </button>
        <button onClick={() => dispatchAction(setPoint())}>.</button>
      </div>
      <div className={styles.opKeyContainer}>
        <button onClick={() => dispatchAction(operation(OPERATION_TYPE.ADD))}>+</button>
        <button onClick={() => dispatchAction(operation(OPERATION_TYPE.MINUS))}>-</button>
        <button onClick={() => dispatchAction(operation(OPERATION_TYPE.MULTIPLY))}>x</button>
        <button onClick={() => dispatchAction(operation(OPERATION_TYPE.DIVIDE))}>/</button>
        <button onClick={() => dispatchAction(operation(OPERATION_TYPE.SQRT))}>√</button>
        <button onClick={() => dispatchAction(operation(OPERATION_TYPE.SUM))}>Σ</button>
        <button onClick={() => dispatchAction(hitUndo())}>Undo</button>
        <button onClick={() => dispatchAction(hitIntro())}>Intro</button>
      </div>
      <div className={styles.stack}>{stack.map(renderStackItem)}</div>
    </div>
  );
};
